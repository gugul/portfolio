-- Zadanie 1
SELECT last_name, department_id, manager_id
FROM employees;

-- Zadanie 2
SELECT * FROM	 employees;

-- Zadanie 3
SELECT max_salary * 12 AS "maximal annual income"
FROM jobs
WHERE job_title LIKE "Programmer";

-- Zadanie 4
SELECT (min_salary + 300) * 12 AS "minimal annual income"
FROM jobs
WHERE job_title LIKE "AdminIStration AssIStant";

-- Zadanie 5
SELECT last_name AS NAZWISKO, salary AS PENSJA
FROM employees;

-- Zadanie 6
SELECT concat(first_name," ", last_name) AS PRACOWNIK
FROM employees;

-- Zadanie 7
SELECT concat(last_name," ","pracuje na stanowISku"," ", job_id)
FROM employees;

-- Zadanie 8
SELECT department_id
FROM employees;

-- Zadanie 9
SELECT DISTINCT department_id
FROM employees;

-- Zadanie 10
SELECT DISTINCT department_id, job_id 
FROM employees;

-- Zadanie 11
SELECT *
FROM jobs
ORDER BY job_title;

-- Zadanie 12
SELECT *
FROM employees
ORDER BY salary DESC;

-- Zadanie 13
SELECT *
FROM employees
ORDER BY salary DESC, last_name ASC;

-- Zadanie 14
SELECT employee_id, last_name, salary
FROM employees
WHERE salary > 2000;

-- Zadanie 15
SELECT *
FROM employees
WHERE salary between 3000 AND 9000;

-- Zadanie 16
SELECT last_name, manager_id
FROM employees
WHERE manager_id in(100,120,146);

-- Zadanie 17
SELECT *
FROM departments
WHERE department_name LIKE "C%";

-- Zadanie 18
SELECT *
FROM countries
WHERE country_name LIKE "_____";

-- Zadanie 19
SELECT *
FROM employees
WHERE commISsion_pct IS NOT NULL;

-- Zadanie 20
SELECT * 
FROM departments
WHERE department_name LIKE "C%";

-- Zadanie 21
SELECT *
FROM employees
WHERE commISsion_pct IS NULL;

-- Zadanie 22
SELECT *
FROM employees
WHERE job_id LIKE "ST_MAN" 
AND salary >= 7900
AND salary < 8200;

-- Zadanie 23
SELECT *
FROM employees
WHERE job_id LIKE "ST_MAN"
AND salary >= 7900
AND salary < 8200
OR job_id LIKE "IT_PROG";

-- Zadanie 24
SELECT *
FROM employees
WHERE job_id LIKE "it_prog" OR job_id LIKE "st_clerk"
AND manager_id LIKE "121";

-- Zadanie 25
SELECT *
FROM locations;

-- Zadanie 26
SELECT department_id, department_name
FROM departments
ORDER BY department_id;

-- Zadanie 27
SELECT DISTINCT job_id
FROM employees;

-- Zadanie 28
SELECT *
FROM employees
WHERE department_id = 30 OR department_id = 50
ORDER BY employee_id DESC;

-- Zadanie 29
SELECT *
FROM employees
WHERE last_name LIKE "%th%" OR last_name LIKE "%ll%";

-- Zadanie 30
SELECT first_name, last_name, hire_date
FROM employees
WHERE hire_date > "2006-12-30"




