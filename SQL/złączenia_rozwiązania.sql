-- 1
SELECT *
FROM employees e, departments d
WHERE e.department_id=d.department_id;
-- 2
SELECT *
FROM employees e
INNER JOIN departments d ON d.department_id=e.department_id;
-- 3
SELECT country_name, regiON_name
FROM countries c
JOIN regiONs r ON c.regiON_id = r.regiON_id;
-- 4
SELECT *
FROM countries c
JOIN locatiONs l ON c.country_id = l.country_id
WHERE c.country_name LIKE "%a";
-- 5
SELECT  first_name, lASt_name, min(salary)
FROM employees;
-- 6
SELECT first_name, lASt_name, country_name
FROM employees e
JOIN departments d ON e.department_id=d.department_id
JOIN locatiONs l ON d.locatiON_id=l.locatiON_id
JOIN countries c ON l.country_id=c.country_id;
-- 7
SELECT pracownik.lASt_name AS pracownik, szef.lASt_name AS szef
FROM employees pracownik JOIN employees szef 
ON pracownik.manager_id=szef.employee_id;
-- 8
SELECT pracownik.lASt_name AS pracownik, szef.lASt_name AS szef
FROM employees pracownik LEFT JOIN employees szef 
ON pracownik.manager_id=szef.employee_id;
-- 9
SELECT IFNULL(departments.department_name, "brak"), IFNULL(employees.first_name, "bez departamentu"), IFNULL(employees.lASt_name, "brak")
FROM employees RIGHT JOIN departments
ON employees.department_id = departments.department_id
ORDER BY department_name;
-- 10
SELECT City_name, regiON_name, country_name
FROM countries, regiONs;
-- 11
SELECT first_name, lASt_name, job_id, department_name
FROM employees 
JOIN departments ON employees.department_id = departments.department_id;
-- 12
SELECT department_name, employees.first_name, employees.lASt_name
FROM  departments 
LEFT JOIN employees ON employee_id = departments.manager_id;
-- 13
SELECT first_name, lASt_name, city
FROM employees
LEFT JOIN departments ON employees.department_id = departments.department_id
JOIN locatiONs ON departments.locatiON_id = locatiONs.locatiON_id;
-- 14
SELECT first_name, lASt_name, country_name
FROM employees
LEFT JOIN departments ON employees.department_id = departments.department_id
JOIN locatiONs ON departments.locatiON_id = locatiONs.locatiON_id
JOIN countries ON countries.country_id = locatiONs.country_id;
-- 15
SELECT first_name, lASt_name, salary, max_salary, max_salary - salary AS "do maksymalnej pensji brakuje"
FROM employees
LEFT JOIN jobs ON employees.job_id = jobs.job_id
WHERE max_salary - salary >= 0



