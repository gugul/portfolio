SELECT *
FROM CITY_ADAM_P;
-- 1
INSERT INTO CITY_ADAM_P(City_id, city_name, population)
VALUES (0, "Warszawa", 1711000);
-- 2.2
INSERT INTO CITY_ADAM_P
VALUES (1, "Łódz", 722022);
-- 2.3
INSERT INTO CITY_ADAM_P
VALUES (2, "Kraków", 759131);
-- 2.4
INSERT INTO CITY_ADAM_P
VALUES (3, "Wrocław", 631377);
-- 2.5
INSERT INTO CITY_ADAM_P
VALUES (4, "Poznań", 552393);
-- 3
UPDATE CITY_ADAM_P
SET Population = 800000
WHERE City_name = "Kraków";
-- 4
UPDATE CITY_ADAM_P
SET Population = 142522
WHERE City_name = "Wrocław";
-- 5
UPDATE CITY_ADAM_P
SET Population = Population + 90341;
-- 6
DELETE FROM CITY_ADAM_P
WHERE City_name = "Warszawa";
-- 7
DELETE FROM CITY_ADAM_P;

